Image Classification
Image  Classification  is  an important application in Computer Vision domain.
It is done  using a neural network that uses image as input.
Image can be gray scale (Only 1 channel) or RGB (3 channels).
The neural network is termed as Convolutional Neural network, shortly abbreviated as CNN.
We will explain the Image Classification problem by using the following example, shown below.

1. As shown in the figure, we have taken RGB image of a woman smiling. The dimensions of image are 300 x 204 x 3.
2. The CNN model will use pixels of the input image to extract features from it and then use it for obtaining useful information.
3. We will classify the image into one of the 7 emotion classes namely angry, happy, neutral, fear, disgust, sad and surprised.
4. In this first convonet layer, 10 filters of size 3 x 3 are used to extract 10 local feature maps (edges,curves).
5. Then a max pooling layer , containg 10 filters of size 2 x 2 is applied to extract features more deeply, decresing the resolution.
6. Then we again apply a sequence of convonet and a max pooling layer to extract global feature maps.
7. Then we apply a flatten layer , so all values are stored in a single vector with only one column.
8. Then we subsequently apply two fully connected layers .
9. And then in the final output layer containing 10 output neurons , we feed output values of previous fully connected layer.
10. Accordingly, to the final layer , the output neuron which classifies happy emotion , has the maximum probability  value  ( 0.95) than the rest neurons. Thus, we say that the CNN model has successfully classified  that the image belongs to happy class.
This is called Image Classification.